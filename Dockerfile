FROM httpd:2.4.51-buster

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8


# update and install packages
RUN apt-get -q update && \
    apt-get -qy --allow-downgrades --allow-remove-essential --allow-change-held-packages upgrade && \
    apt-get install -y planet-venus procps curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add venus user
RUN adduser --quiet --disabled-password --shell /bin/bash --home /home/venus --gecos "User" venus

# Enable cron
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

COPY cron.txt /home/venus/

# Clean default Apache sites
RUN rm -rf /usr/local/apache2/htdocs/*

# Copy Apache config
COPY httpd.conf /usr/local/apache2/conf/httpd.conf

# Copy directories
COPY faces /usr/local/apache2/htdocs/faces
COPY graphics /usr/local/apache2/htdocs/graphics
COPY look /usr/local/apache2/htdocs/look
COPY fonts /usr/local/apache2/htdocs/fonts
COPY theme /home/venus/theme
COPY lang /home/venus/lang
COPY bin /home/venus/bin
RUN touch /home/venus/planet.log

# Set correct permissions
RUN chown -R venus:venus /usr/local/apache2/htdocs /usr/local/apache2/logs /home/venus/
RUN chmod 644 /usr/local/apache2/conf/httpd.conf

# Switch to non-privileged user
USER venus

# Link EN as the default page
WORKDIR /usr/local/apache2/htdocs/
RUN ln -s en/atom.xml en/foafroll.xml en/index.html en/opml.xml en/rss20.xml .

# Ugly hack to break caching from here on
ADD https://status.fsfe.org/fsfe.org/ /dev/null

# Initially run all planets
RUN find /home/venus/lang/ -name "planet_*.ini" -exec planet {} \;

# Run Apache and supercronic
CMD httpd -DFOREGROUND -f /usr/local/apache2/conf/httpd.conf & supercronic /home/venus/cron.txt
